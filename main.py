__author__ = 'sammoth'

import os
from datetime import datetime, tzinfo

output_file = "../test_log.txt"

NuBot_Path_1 = "../test_logs/"
NuBot_Path_2 = ""

NuBot_Paths = [NuBot_Path_1, NuBot_Path_2]

for NuBot_Path in NuBot_Paths:
	if NuBot_Path == "":
		continue



	#Get the newest sub-directory of the logs folder
	all_log_dirs = []
	for d in os.listdir(NuBot_Path):
		bd = os.path.join(NuBot_Path, d)
		if os.path.isdir(bd):
			all_log_dirs.append(bd)
	latest_log_dir = max(all_log_dirs, key=os.path.getmtime)

	#build the paths to the files of interest
	wall_shifts = os.path.join(latest_log_dir, 'wall_shifts.csv')
	orders_history = os.path.join(latest_log_dir, 'orders_history.csv')

	print(wall_shifts)	
	
	#build a list of the file names for iteration
	files = [wall_shifts, orders_history]

	#set up a Datetime object for five minutes ago
	now = datetime.now()

	#open the output file
	with open(output_file, 'w+') as output:

		for file in files:
			#check the file exists
			if os.path.isfile(file):
				#read the wall_shifts csv file
				with open(file, 'r') as openFile:
					#remove the header line
					count = 0
					for line in openFile:
						if count == 0:
							count = 1
							continue
						data = line.split(',')

						#check the time
						#file contains - Mon Nov 03 13:03:59 EST 2014
						#                Thu Nov 06 14:18:26 EST 2014 ,
						if data[0] == '\n':
							continue
						print(data[0])
						line_datetime = datetime.strptime(data[0].strip(), "%a %b %d %H:%M:%S EST %Y")
						difference = now - line_datetime
						if difference.seconds > 300:
							continue

						output.write(line)

				openFile.close()

	output.close()










